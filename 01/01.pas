program AoC01;

uses CRT;

VAR
Input: Text;
Zeile,Pfad: shortString;
a,b,c,c0: smallInt;


BEGIN


  clrscr;
  a := 1010;
  b := 1010;
  c := 0;
  c0 := 0;

  writeln('Pfad zur Input-Datei:');
  readln(Pfad);

  assign (Input, Pfad);
  reset (Input);
    REPEAT
    BEGIN
    c0 := c0 + 1;

    REPEAT
      c := c + 1;
      Readln(Input, Zeile);
      writeln(Zeile);

      if c = c0 then a := strtoint(Zeile);

      if c > c0 then if a + strtoint(Zeile) = 2020 then b := strtoint(Zeile);

    UNTIL EOF (Input);

    c := 0;
    END;
  UNTIL a+b = 2020;

  close(Input);


  writeln('A: ',a);
  writeln('B: ',b);
  writeln('A*B: ',a*b);

  readKey;


END.