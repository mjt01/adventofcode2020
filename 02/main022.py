file = open('input.txt')
e = 0
for element in file:
    x,c = element.split(': ')
    a,l = x.split()
    a1,a2 = a.split('-')
    a1 = int(a1)-1
    a2 = int(a2)-1

    f = c.find(l, a1)
    g = c.find(l, a2)
    if a1 == f and a2 != g or a1 != f and a2 == g:
        e = e + 1
file.close()
print(e)